package modificadores;

/**
 * Created by USER on 07/05/2017.
 */
public class cliente {

    public static void main(String[] args) {
        Persona persona = new Persona();
        /*en una clase diferente con la instancia de
        la clase persona tambien se puede acceder a la variable
        publica que creamos anteriormente*/
        persona.VarPublica="accesido 2 ";

        /*para la variable private no pudeo acceder a desde otra clase u otro paquete
        para poder usar la variable private se debe crear metodos get y set,los cuales
        seran publicos y a estos si podra hacer uso */
        persona.VarPrivate ="";// debido a ello nos da el error

        //para el modificador final, se pueda llamar a la clase final siempre y cuando no se intenten cambiar su valor o  dara error
        System.out.print(persona.constante);

        /* con una variable de modificador protected si se puede utilizar
        * en otra clase del mismo paquete*/
        persona.VarProtected = "accesido 2";

    }
}