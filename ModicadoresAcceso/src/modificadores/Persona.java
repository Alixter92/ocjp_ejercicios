package modificadores;

/**
 * Created by USER on 07/05/2017.
 */
public class Persona {

    //declaramos la variable con el modificador de acceso 'public'
    public String VarPublica;

    //declaramos la variable con modificador de acceso  'private'
    private String VarPrivate;

    //declaramos la variable con modificador de acceso 'final'
    //se debe inicializar la variable final ya que necesita tener un valor el cual no cambiara
    final String constante = "12";

    //modificador de acceso 'protected'
    protected String VarProtected;

    //modificador de acceso 'static'
    static String contador= "1";


    public  static void main (String [] args){
        Persona persona = new Persona();
        //si se puede utilizar la variable VarPublica ya que es publica
        persona.VarPublica = "Accesida";

        //como se encuentra en la misma clase en donde se declaro si se puede acceder a esta variable
        persona.VarPrivate = "Accesida";

        /*para la variable final su valor es constante por lo que no se
        puede modificar y solo se puede utilzar para mostrar su valor*/
        persona.constante="";//al querer cambiar el valor nos da un error

        //la diferencia entre protected y private es que si se puede utilzar otra clase del mismo paquete
         persona.VarProtected = "Accesido";

         //el modificador de acceso 'static' es a nivel de instancia, es decir que sera una variable que se puede utilizar de manera compartida
        //el valor con el que se instanciara sera usado por todos los usuarios que accedan a esta variable y se modificara de acuerdo a su uso
    }

}
