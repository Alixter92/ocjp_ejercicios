package modificador2;

import modificadores.Persona;

/**
 * Created by USER on 07/05/2017.
 */
public class usuario {

    public static void main(String[] args){

        Persona persona =new Persona();
        /*en este paquete tambien se puede acceder
        * a la variable publica que creamos en el paquete
        * de modificadores*/
        persona.VarPublica = "tambien se accede";

        /*para la variable private no pudeo acceder a desde otra clase u otro paquete
        para poder usar la variable private se debe crear metodos get y set,los cuales
        seran publicos y a estos si podra hacer uso */


        /*la variable final tampoco se puede usar en otra clase de otro paquete diferente
        donde fue instanciada*/

        /*la variable con el metodo protected  tampoco se puede acceder desde otra clase
        de una paquete diferente */

    }

}
