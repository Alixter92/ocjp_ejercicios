package com.company;

/**
 * Created by USER on 14/05/2017.
 */

// esta es la implementacion de la clase interface
public abstract class Animal implements claseInterface {

    public abstract void hablar();
}


///creamos una Perro que se sera extendida de Animal por lo que hereda de la claseInterface

class Perro extends Animal{
    public void hablar(){
        System.out.print("Guau");
    }
}

