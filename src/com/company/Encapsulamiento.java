package com.company;

/**
 * Created by USER on 14/05/2017.
 */
public class Encapsulamiento {

    //El encapsulamiento permite definir variables de un tipo especial al que no todos los usuarios tengan acceso, si
    //si no solo a a sus metodos get y set
    private int var;
        public void setVar(int t){

            var = t;
        }

    // creamos el metodo get para se pueda hacer uso de la variable var
    public int getVar(){
            return var;
    }
}
