package com.company;

/**
 * Created by USER on 14/05/2017.
 */
public class Sobrecarga {

    /// como se puede ver se tiene el mismo metodo co diferentes parametros
    //de entrada lo que hace que este metodo se encuentre sobrecargado
    //es importante saber que el orden de los parametros tambien es una sobrecarga
    public void cambiarTamano(int tamano, String nombre, double patron){}

    public void cambiarTamano(int tamano, double patron){
        System.out.println("El resultado es:"+tamano+ patron);
    }

    public void cambiarTamano(double patron, String nombre){
        System.out.println("El resultado es:"+patron+ nombre);
    }

    public void cambiarTamano(String nombre, boolean verdad) {
        System.out.println("El resultado es:" + nombre + verdad);


    }

}
