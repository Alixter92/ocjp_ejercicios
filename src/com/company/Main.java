package com.company;

public class Main {

    public static void main(String[] args) {

        ////USO DE ENCAPSULAMIENTO

        Encapsulamiento enc = new Encapsulamiento();
                enc.setVar(5);
                System.out.println("la variable es:"+enc.getVar());

        /////SOBRECARGA DE METODOS
        Sobrecarga s = new Sobrecarga();
        s.cambiarTamano(2, " Dario");
        s.cambiarTamano(2  ,4.5);
        s.cambiarTamano("OCJP ", true);

        //USO DE SOBREESCRITURA DE METODOS
        sobreescritura se = new sobreescritura();
        se.cambiarTamano(5,"dario",2.5);


        //USO DE CONSTRUCTORES
        Persona p= new Persona(25,"alex","M");


        //USO DE  ENUMS
        Precios a = Precios.CARO;
        Precios b = Precios.BARATO;
        Precios c = Precios.NORMAL;

        //devuelve el nombre del enumerador
        System.out.println("el precio es: "+b.name());
        //devuelve la posiscion del enumerador al que se hace referencia
        System.out.println("La posicion del enum NORMAL es: "+c.ordinal());
        //imprime todos los enume disponibles
        for(Precios d:Precios.values()){
            System.out.print(d.toString()+"-");
        }


        ////USO DE HERENCIA
        //como vemos 'he' hereda de la clase herencia los atributos declarados
        Herencia he = new Herencia("Alex","Naranjo",25);





    }
}
